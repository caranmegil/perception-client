/*
Copyright (C) 2022  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import React, { useEffect, useState } from 'react';
import { useApolloClient } from '@apollo/react-hooks';
import './styles.css';
import getSensorDataQuery from '../../queries/getSensorData';
import getTotalRainfallQuery from '../../queries/getTotalRainfall';

const MM_PER_CLICK = .254;
const INCHES_PER_CLICK = .01;

function SensorData() {
    const [sensorData, setSensorData] = useState({
        getSensorData: {
            temperato: 0.00,
        }
    })

    const [totalRainfallData, setTotalRainfallData] = useState({
        getTotalRainfall: 0,
    })
    const client = useApolloClient();

    useEffect(() => {
        async function queryAPI() {
            try {
                let result = await client.query({
                    query: getSensorDataQuery,
                    fetchPolicy: 'no-cache',
                });
                setSensorData(result.data);

                result = await client.query({
                    query: getTotalRainfallQuery,
                    fetchPolicy: 'no-cache',
                });
                setTotalRainfallData(result.data);
            } catch(e: any) {
                console.error(e);
            }
        }

        const intervalId = setInterval( async () => {
            await queryAPI();
        }, 3000);

        return () => clearInterval(intervalId);        
    }, [client]);

    return (
        <div>
            <p>
                <header>Temperato</header>
                <div>{Math.round((sensorData.getSensorData.temperato * 9 / 5) + 32)} F / {Math.round(sensorData.getSensorData.temperato)} C</div>
            </p>
            <p>
                <header>Pluviam</header>
                <div>{Math.round(totalRainfallData.getTotalRainfall * 100 * INCHES_PER_CLICK) / 100} inches / {Math.round(totalRainfallData.getTotalRainfall * 1000 * MM_PER_CLICK) / 1000} mm of water</div>
            </p>
        </div>
    );
}

export default SensorData;