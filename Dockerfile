FROM node:18-alpine3.14
RUN apk update && apk upgrade && sync
EXPOSE 3000
WORKDIR /usr/perception-client
COPY . /usr/perception-client
RUN npm ci && npm run build
CMD ["npm", "run", "serve"]